# Proto
> Protocol buffer implementation

* [Services](#services)
  * [Posts](#posts)
    * [Add](#add)
    * [Get](#get)
    * [Delete](#delete)
  * [Users](#users)
    * [Add](#add-1)
    * [Get](#get-1)
    * [Login](#login)
    * [Atoken](#atoken)
    * [Rtoken](#rtoken)
    * [Validate](#validate)
* [Messages](#messages)
  * [Jwt](#jwt)
  * [Post](#post)
  * [TokenizedPost](#tokenizedpost)
  * [User](#user)

**Last version**: 1.0.0

## Services

### Posts

Posts service definition

#### Add

Adds a new post

```proto
rpc Add (TokenizedPost) returns (Post) {}
```

#### Get

Gets a post

```proto
rpc Get (TokenizedPost) returns (Post) {}
```

#### Delete

Deletes a post

```proto
rpc Delete (TokenizedPost) returns (google.protobuf.Empty) {}
```

### Users

Users service definition

#### Add

Adds a new user

```proto
rpc Add (User) returns (User) {}
```

#### Get

Gets a user

```proto
rpc Get (User) returns (User) {}
```

#### Login

Logins a user

```proto
rpc Login (User) returns (Jwt) {}
```

#### Atoken

Returns an access token

```proto
rpc Atoken (Jwt) returns (Jwt) {}
```

#### Rtoken

Returns a refresh token

```proto
rpc Rtoken (Jwt) returns (Jwt) {}
```

#### Validate

Validates a token

```proto
rpc Validate (Jwt) returns (User) {}
```

## Messages

### Jwt

```proto
message Jwt {
    string token = 1;
    google.protobuf.Timestamp exp = 2;
    google.protobuf.Timestamp iat = 3;
}
```

### Post

```proto
message Post {
    string post_uuid = 1;
    string owner = 2;
    string title = 3;
    google.protobuf.Timestamp created_at = 4;
}
```

### TokenizedPost

```proto
message TokenizedPost {
    Post post = 1;
    Jwt token = 2;
}
```

### User

```proto
message User {
    string user_uuid = 1;
    string username = 2;
    string password = 3;
    google.protobuf.Timestamp created_at = 4;
}
```
